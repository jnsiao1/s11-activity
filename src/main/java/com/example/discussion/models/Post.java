package com.example.discussion.models;
import org.hibernate.annotations.CollectionId;

import javax.persistence.*;

@Entity(name="post")
@Table
public class Post {
    //Properties
    //Indicates that this property the primary of the table
    @Id
    //Values of this property will be incremented
    @GeneratedValue
    private long id;
    //Class properties that represent the table  columns in a relational database are annotated as @Column
    @Column
    private String title;
    private String content;

    //constructors
    public Post(){};

    public Post(String title, String content){
        this.title = title;
        this.content =content;
    }
    //Getters and Setters

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}